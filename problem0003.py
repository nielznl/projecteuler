#!/usr/bin/python3

import itertools

'''

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

'''


def isPrime(possiblePrime):
    if possiblePrime < 2:
        return False

    for number in itertools.islice(itertools.count(2), int(possiblePrime ** 0.5 - 1)):
        if possiblePrime % number == 0:
            return False

    return True


def main():
    number = 600851475143
    largestPrimeOfNumber = 0
    for possiblePrime in range(1, int(number ** 0.5), 2):
        if number % possiblePrime == 0 and isPrime(possiblePrime):
            largestPrimeOfNumber = possiblePrime
    return largestPrimeOfNumber


if __name__ == "__main__":
    print(main())
