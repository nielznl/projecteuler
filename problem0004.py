#!/usr/bin/python3

import math


'''

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

'''


def main():
    highestProduct = 0
    for firstDigit in range(100, 1000):
        for secondDigit in range(100, firstDigit):
            number = str(firstDigit * secondDigit)
            firstPart = number[::-1][0:math.ceil(len(number) / 2)]
            secondPart = number[0:math.ceil(len(number) / 2)]
            if firstPart == secondPart and int(number) > highestProduct:
                highestProduct = int(number)
    return highestProduct


if __name__ == "__main__":
    print(main())
