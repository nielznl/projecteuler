#!/usr/bin/python3

import itertools

'''

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

'''


def main():
    primeMultiple = 2 * 3 * 5 * 7 * 11 * 13 * 17 * 19
    for i in itertools.count(1):
        possibleNumber = i * primeMultiple
        for division in range(1, 21):
            if possibleNumber % division != 0:
                break
        else:
            break
    return possibleNumber


if __name__ == "__main__":
    print(main())
