#!/usr/bin/python3

'''

The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

'''


def squareOfSum(numberCount):
    answer = 0
    for x in range(1, (numberCount + 1)):
        answer = answer + x
    answer = answer * answer
    return answer


def sumOfSquares(numberCount):
    answer = 0
    for x in range(1, (numberCount + 1)):
        answer = answer + (x * x)
    return answer


def main():
    return squareOfSum(100) - sumOfSquares(100)


if __name__ == "__main__":
    print(main())
