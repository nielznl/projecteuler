#!/usr/bin/python3

import itertools

'''

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

'''


def isPrime(possiblePrime):
    if possiblePrime < 2:
        return True

    for number in itertools.islice(itertools.count(2), int(possiblePrime ** 0.5)):
        if possiblePrime % number == 0:
            return False

    return True


def main():
    primesFound = 0
    for possiblePrime in itertools.count(1):
        if isPrime(possiblePrime):
            primesFound += 1
            if primesFound == 10001:
                break
    return possiblePrime


if __name__ == "__main__":
    print(main())
