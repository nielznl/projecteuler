#!/usr/bin/python3

import json

'''

The four adjacent digits in the 1000-digit number that have the greatest product are 9 × 9 × 8 × 9 = 5832.

Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?

'''


def importNumber():
    numberFile = open("resources/resource0008.json", "r")
    numberText = numberFile.read()
    number = json.loads(numberText)[0]
    return(str(number))


def main():
    importNumber()
    number = importNumber()
    amountOfDigits = 13
    highestProduct = 0
    for adjacentDigitsPosition in range(0, len(number) - amountOfDigits + 1):
        adjacentDigits = number[adjacentDigitsPosition:adjacentDigitsPosition + amountOfDigits]
        product = 1
        for digit in range(0, amountOfDigits):
            product = int(adjacentDigits[digit]) * product
        if (product > highestProduct):
            highestProduct = product
    return highestProduct


if __name__ == "__main__":
    print(main())
