#!/usr/bin/python3

'''

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

'''


def main():
    for a in range(2, 1000):
        for b in range(2, 1000):
            c = 1000 - a - b
            if (c > 0):
                if ((a * a) + (b * b) == (c * c)):
                    answer = a * b * c
    return answer


if __name__ == "__main__":
    print(main())
