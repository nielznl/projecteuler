#!/usr/bin/python3

import itertools

'''

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

'''


def getPrimeSum(limit):
    A = [True for x in range(0, limit)]
    for i in range(2, int(limit ** 0.5)):
        if A[i] is True:
            for j in itertools.count(0):
                j = i * 2 + j * i
                if j < limit:
                    A[j] = False
                else:
                    break
    primes = []
    for i in range(2, len(A)):
        if A[i] is True:
            primes.append(i)
    return primes


def main():
    primes = getPrimeSum(2000000)
    return sum(primes)


if __name__ == "__main__":
    print(main())
