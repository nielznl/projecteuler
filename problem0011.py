#!/usr/bin/python3

import functools
import operator
import json

'''

In the 20×20 grid below, four numbers along a diagonal line have been marked in red.

The product of these numbers is 26 × 63 × 78 × 14 = 1788696.

What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?

'''


def extractNumbers():
    rawNumberLines = importNumbers()
    numberList = []
    for line in rawNumberLines:
        numberList.append(line.split(" "))
    numbers = numberList
    return numbers


def importNumbers():
    numberFile = open("resources/resource0011.json", "r")
    return json.loads(numberFile.read())


def extractAdjacents(numbers):
    adjacents = []
    # extract horizontal adjacents
    for y in numbers:
        for x in zip(y[:-3], y[1:], y[2:], y[3:]):
            adjacents.append(x)

    # extract vertical adjacents
    for y in zip(numbers[:-3], numbers[1:], numbers[2:], numbers[3:]):
        for x in zip(y[0], y[1], y[2], y[3]):
            adjacents.append(x)

    # extract diagonally [\] adjacents
    for y in zip(numbers[:-3], numbers[1:], numbers[2:], numbers[3:]):
        for x in zip(y[0][:-3], y[1][1:], y[2][2:], y[3][3:]):
            adjacents.append(x)

    # extract more diagonally [/] adjacents
    for y in zip(numbers[3:], numbers[2:], numbers[1:], numbers[0:]):
        for x in zip(y[0][:-3], y[1][1:], y[2][2:], y[3][3:]):
            adjacents.append(x)
    return adjacents


def getHighestNumber(adjacents):
    highestNumber = 0
    for numbers in adjacents:
        numberProduct = functools.reduce(operator.mul, (map(int, numbers)))
        if numberProduct > highestNumber:
            highestNumber = numberProduct
    return highestNumber


def main():
    numbers = extractNumbers()
    adjacents = extractAdjacents(numbers)
    highestNumber = getHighestNumber(adjacents)
    return highestNumber


if __name__ == "__main__":
    print(main())
