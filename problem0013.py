#!/usr/bin/python3

'''

Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.

'''


def importNumbers():
    numbers = open("resources/resource0013.txt").read()
    return numbers


def main():
    numbers = importNumbers()
    numberList = numbers.split("\n")
    numberSum = 0
    for number in numberList:
        numberSum += int(number)
    answer = str(numberSum)[0:10]
    return answer


if __name__ == "__main__":
    print(main())
