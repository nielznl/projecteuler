#!/usr/bin/python3

'''

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem),
it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

'''


NUMBERS = {1: (None, 1)}


def fillNumbers(limit, LONGEST_CHAIN_NUMBER):
	for number in range(2, limit):
		LONGEST_CHAIN_NUMBER = getNextNumber(number, LONGEST_CHAIN_NUMBER)
	return LONGEST_CHAIN_NUMBER


def getNextNumber(number, LONGEST_CHAIN_NUMBER):
	if number not in NUMBERS:
		if number % 2 == 0:
			nextNumber = number // 2
		else:
			nextNumber = number * 3 + 1
		if nextNumber in NUMBERS:
			chainLength = NUMBERS[nextNumber][1] + 1
			NUMBERS[number] = (nextNumber, chainLength)
		else:
			getNextNumber(nextNumber, LONGEST_CHAIN_NUMBER)
			chainLength = NUMBERS[nextNumber][1] + 1
			NUMBERS[number] = (nextNumber, chainLength)
		if LONGEST_CHAIN_NUMBER[1] < chainLength:
			LONGEST_CHAIN_NUMBER = [number, chainLength]
	return LONGEST_CHAIN_NUMBER


def main():
	LONGEST_CHAIN_NUMBER = [1, 1]  # [number, chainLength]
	LONGEST_CHAIN_NUMBER = fillNumbers(1000000, LONGEST_CHAIN_NUMBER)
	answer = LONGEST_CHAIN_NUMBER[0]
	return answer


if __name__ == "__main__":
	print(main())
