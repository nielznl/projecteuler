#!/usr/bin/python3

'''

Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?

'''


def getNextRoute(routes):
    gridPos = len(routes)
    if gridPos == 0:
        routes.append([2])
    else:
        row = []
        for pos in range(0, gridPos + 1):
            if pos == 0:
                row.append(gridPos + 2)
            elif pos == gridPos:
                row.append(row[-1] * 2)
            else:
                row.append(row[-1] + routes[-1][pos])
        routes.append(row)
    return routes


def main():
    routes = []
    while len(routes) < 20:
        getNextRoute(routes)
    answer = routes[-1][-1]
    return answer


if __name__ == "__main__":
    print(main())
