#!/usr/bin/python3

'''

215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 21000?

'''


def main():
    digits = list(str(2**1000))
    # convert string digits to integers
    digits = list(map(int, digits))
    answer = sum(digits)
    return answer


if __name__ == "__main__":
    print(main())
