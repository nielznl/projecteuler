#!/usr/bin/python3

'''

If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
The use of "and" when writing out numbers is in compliance with British usage.

'''


SINGLE_NUMBERS = {
    "0": "",
    "1": "one",
    "2": "two",
    "3": "three",
    "4": "four",
    "5": "five",
    "6": "six",
    "7": "seven",
    "8": "eight",
    "9": "nine",
}

SPECIAL_TEN_NUMBERS = {
    "11": "eleven",
    "12": "twelve",
    "13": "thirteen",
    "14": "fourteen",
    "15": "fifteen",
    "16": "sixteen",
    "17": "seventeen",
    "18": "eighteen",
    "19": "nineteen"
}

TENS_NUMBERS = {
    "10": "ten",
    "20": "twenty",
    "30": "thirty",
    "40": "forty",
    "50": "fifty",
    "60": "sixty",
    "70": "seventy",
    "80": "eighty",
    "90": "ninety",
}


def main():
    usedLetters = 0
    oneToNinetyNine = []
    # add 1 to 19
    for number, word in dict(dict(SINGLE_NUMBERS, **{"10": TENS_NUMBERS["10"]}), **SPECIAL_TEN_NUMBERS).items():
        oneToNinetyNine.append(word)
        usedLetters += len(word)
    # add 20 to 99
    for number, word in list(TENS_NUMBERS.items())[1:]:
        for singleNumber, singleWord in SINGLE_NUMBERS.items():
            fullWord = word + singleWord
            oneToNinetyNine.append(fullWord)
            usedLetters += len(fullWord)
    # add 100 to 999
    for hundredNumber, hundredWord in list(SINGLE_NUMBERS.items())[1:]:
        for dualWord in oneToNinetyNine:
            if dualWord == "":
                fullWord = hundredWord + "hundred" + dualWord
            else:
                fullWord = hundredWord + "hundredand" + dualWord
            usedLetters += len(fullWord)
    # add 1000
    usedLetters += len("onethousand")
    answer = usedLetters
    return answer


if __name__ == "__main__":
    print(main())
