#!/usr/bin/python3

'''

By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle.

NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67,
is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)

'''


def parseTriangle():
    triangleLines = open("resources/resource0018.txt", "r").read()
    triangleLines = list(filter(None, triangleLines.split("\n")))
    triangleList = []
    for row in triangleLines:
        triangleList.append(list(map(int, row.split(" "))))
    return(triangleList)


def main():
    triangle = parseTriangle()
    for rowPos in range(1, len(triangle)):
        totalTriangleRow = []
        # Adding the first digit of the previous row to the first digit
        totalTriangleRow.append(triangle[rowPos][0] + triangle[rowPos - 1][0])
        # Everything between the first and last digit
        for numPos in range(1, len(triangle[rowPos]) - 1):
            highestNumber = max(triangle[rowPos - 1][numPos], triangle[rowPos - 1][numPos - 1])
            totalTriangleRow.append(triangle[rowPos][numPos] + highestNumber)
        # Adding the last digit of the previous row to the last digit
        totalTriangleRow.append(triangle[rowPos][-1] + triangle[rowPos - 1][-1])
        triangle[rowPos] = totalTriangleRow
    answer = max(triangle[-1])
    return answer


if __name__ == "__main__":
    print(main())
