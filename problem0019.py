#!/usr/bin/python3

'''

You are given the following information, but you may prefer to do some research for your

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

'''


MONTHS = {
    "January": 31,
    "February": 28,
    "March": 31,
    "April": 30,
    "May": 31,
    "June": 30,
    "July": 31,
    "August": 31,
    "September": 30,
    "October": 31,
    "November": 30,
    "December": 31,
}


def main():
    days = 0
    sundaysOnTheFirst = 0
    for year in range(1901, 2001):
        months = dict(MONTHS)
        if year % 400 == 0:
            months["February"] += 1
        elif year % 100 == 0:
            pass
        elif year % 4 == 0:
            months["February"] += 1
        for month in months:
            if days % 7 == 0:
                sundaysOnTheFirst += 1
            days += months[month]
    answer = sundaysOnTheFirst
    return answer


if __name__ == "__main__":
    print(main())
