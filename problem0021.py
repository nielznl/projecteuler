#!/usr/bin/python3

'''

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.

'''


def sumOfDivisors(number):
    divisors = [1]
    for possibleDivisor in range(2, int(number ** 0.5) + 1):
        if number % possibleDivisor == 0:
            divisors.append(possibleDivisor)
            divisors.append(number // possibleDivisor)
    return(sum(divisors))


def isAmicable(number):
    sumOfDivisorsNumber = sumOfDivisors(number)
    sumOfDivisorsAmicable = sumOfDivisors(sumOfDivisorsNumber)
    if sumOfDivisorsNumber != number and sumOfDivisorsAmicable == number:
        return(True)
    return(False)


if __name__ == "__main__":
    amicableNumbers = []
    for number in range(1, 10000):
        if isAmicable(number):
            amicableNumbers.append(number)
    answer = sum(amicableNumbers)
    print(answer)
