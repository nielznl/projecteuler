#!/usr/bin/python3

import json
import string

'''

Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?

'''


def sortNames(names):
    names.sort()
    return names


def importNames():
    namesText = open("resources/resource0022.txt").read()
    names = json.loads("[" + namesText + "]")
    names = sortNames(names)
    return names


def alphabeticalValue(name):
    alphabet = " " + string.ascii_uppercase
    value = 0
    for letter in name:
        letterWorth = alphabet.find(letter)
        value += letterWorth
    return value


if __name__ == "__main__":
    names = importNames()
    total = 0
    for namePos in range(0, len(names)):
        nameWorth = alphabeticalValue(names[namePos])
        score = nameWorth * (namePos + 1)
        total += score
    answer = total
    print(answer)
