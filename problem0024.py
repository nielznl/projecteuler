#!/usr/bin/python3

'''

A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4.
If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

'''


# When I wrote this, only God and I understood what I was doing
# Now, God only knows


def getPerm(digits, position):
    digits.sort()
    permutations = []
    for digitPos in range(len(digits)):
        digit = digits[digitPos]
        remainingDigits = digits[:digitPos] + digits[digitPos + 1:]
        if len(digits) > 1:
            remainingDigitCombinations = getPerm(remainingDigits, position)
        else:
            remainingDigitCombinations = [remainingDigits]
        for remainingDigitCombination in remainingDigitCombinations:
            remainingDigitCombination.insert(0, digit)
            permutations.append(remainingDigitCombination)
            if len(permutations) == position:
                return(remainingDigitCombination)
    return(permutations)


if __name__ == "__main__":
    digits = list(range(10))
    position = 1000000
    answer = getPerm(digits, position)
    answer = "".join(map(str, answer))
    print(answer)
