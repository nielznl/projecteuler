#!/usr/bin/python3

'''

A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

    1/2 =   0.5
    1/3 =   0.(3)
    1/4 =   0.25
    1/5 =   0.2
    1/6 =   0.1(6)
    1/7 =   0.(142857)
    1/8 =   0.125
    1/9 =   0.(1)
    1/10    =   0.1

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.

'''


from decimal import Decimal, getcontext


def getFraction(number, digits):
    getcontext().prec = digits
    fraction = Decimal(1) / Decimal(number)
    fraction = str(fraction)[2:-4]
    return fraction


def recurringCycleLength(number):
    fraction = getFraction(number, 5000)
    if len(fraction) > 100:
        split = fraction.split(fraction[-12:])
        if split[1] == split[2]:
            return(len(split[1]))
    else:
        return(0)


def loopThroughNumbers(highestNumber):
    longestRecurringCycle = 0
    numberWithLongestRecurringCycle = 0
    for number in range(2, highestNumber):
        recurringCycleLen = recurringCycleLength(number)
        if recurringCycleLen > longestRecurringCycle:
            longestRecurringCycle = recurringCycleLen
            numberWithLongestRecurringCycle = number
    return numberWithLongestRecurringCycle


if __name__ == "__main__":
    answer = loopThroughNumbers(1000)
    print(answer)
