#!/usr/bin/python3

'''

Euler discovered the remarkable quadratic formula:

n2+n+41

It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤39
. However, when n=40,402+40+41=40(40+1)+41 is divisible by 41, and certainly when n=41,412+41+41

is clearly divisible by 41.

The incredible formula n2−79n+1601
was discovered, which produces 80 primes for the consecutive values 0≤n≤79

. The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

    n2+an+b

, where |a|<1000 and |b|≤1000

where |n|
is the modulus/absolute value of n
e.g. |11|=11 and |−4|=4

Find the product of the coefficients, a
and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.

'''


import itertools
import math


def maxPrimesQuadratic():
    highestN = 0
    for b in primesBelow(1000):
        for a in range(-1000, 1000):
            for n in itertools.count(1):
                quadratic = quadratics(n, a, b)
                if not isPrime(quadratic):
                    break
            if n >= highestN:
                highestN = n
                highestAB = (a, b)
    return(highestAB)


def quadratics(n, a, b):
    quadratic = n**2 + a * n + b
    return(quadratic)


def isPrime(possiblePrime):
    if possiblePrime < 2:
        return False

    for number in itertools.islice(itertools.count(2), int(math.sqrt(possiblePrime) - 1)):
        if possiblePrime % number == 0:
            return False

    return True


def primesBelow(n):
    for possiblePrime in range(2, n):
        if isPrime(possiblePrime):
            yield(possiblePrime)


if __name__ == "__main__":
    a, b = maxPrimesQuadratic()
    answer = (a * b)
    print(answer)
