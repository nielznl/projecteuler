#!/usr/bin/python3

'''

In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).

It is possible to make £2 in the following way:

    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p

How many different ways can £2 be made using any number of coins?

'''


def possibilities(amountToFill, coins, coinPos=0):
    currentPossibilities = 0
    coin = coins[coinPos]
    maxFittingCoins = amountToFill // coin
    for fittingCoins in range(0, maxFittingCoins + 1):
        amountLeft = amountToFill - fittingCoins * coin
        if amountLeft < 1:
            currentPossibilities += 1
        elif coin == 1:
            continue
        else:
            currentPossibilities += possibilities(amountLeft, coins, coinPos + 1)

    return currentPossibilities


if __name__ == "__main__":
    coins = [200, 100, 50, 20, 10, 5, 2, 1]
    answer = possibilities(200, coins)
    print(answer)
