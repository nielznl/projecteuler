#!/usr/bin/python3

import itertools

'''

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.

'''


if __name__ == "__main__":
    products = set()
    for permutation in itertools.permutations(range(1, 10)):
        permutation = "".join(list(map(str, permutation)))
        possibleProduct = int(permutation[5:])
        if int(permutation[0]) * int(permutation[1:5]) == possibleProduct or int(permutation[0:2]) * int(permutation[2:5]) == possibleProduct:
            products.add(possibleProduct)
    answer = sum(products)
    print(answer)
