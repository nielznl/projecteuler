#!/usr/bin/python3

'''

The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8,
which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

'''


def getSimpleOutcomes(numerator, denominator):
    if int(denominator[1]) == 0:
        return []
    simpleOutcomes = []
    if numerator[0] == denominator[0]:
        outcome = int(numerator[1]) / int(denominator[1])
        simpleOutcomes.append(outcome)
    if numerator[1] == denominator[1]:
        outcome = int(numerator[0]) / int(denominator[0])
        simpleOutcomes.append(outcome)
    if numerator[1] == denominator[0]:
        outcome = int(numerator[0]) / int(denominator[1])
        simpleOutcomes.append(outcome)
    if numerator[0] == denominator[1]:
        outcome = int(numerator[1]) / int(denominator[0])
        simpleOutcomes.append(outcome)
    return(simpleOutcomes)


if __name__ == "__main__":
    numProduct = 1
    denProduct = 1
    for numerator in range(10, 100):
        for denominator in range(numerator + 1, 100):
            outcome = numerator / denominator
            simpleOutcomes = getSimpleOutcomes(str(numerator), str(denominator))
            if outcome in simpleOutcomes:
                numProduct *= numerator
                denProduct *= denominator
    answer = int(denProduct / numProduct)
    print(answer)
