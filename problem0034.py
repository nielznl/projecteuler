#!/usr/bin/python3

'''

145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.

'''


def factorial(maxNumber):
    total = 1
    for number in range(2, maxNumber + 1):
        total *= number
    return(total)


if __name__ == "__main__":
    curiousTotal = 0
    for number in range(10, 100000):
        total = 0
        for digit in str(number):
            fact = factorial(int(digit))
            total += fact
        if total == number:
            curiousTotal += total
    print(curiousTotal)
