#!/usr/bin/python3

import itertools


'''

The number, 197, is called a circular prime because all rotations of the numbers: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?

'''


def getPrimes():
    primes = [2]
    for x in range(3, 1000000, 2):
        for prime in primes[:int(len(primes) ** 0.5) + 1]:
            if x % prime == 0:
                break
        else:
            primes.append(x)
    return primes


def rotations(number):
    yield number
    number = list(str(number))
    for x in range(1, len(number)):
        rotatedDigit = number[x:] + number[:x]
        yield int("".join(rotatedDigit))


def isPrime(possiblePrime):
    if possiblePrime < 2:
        return False

    for number in itertools.islice(itertools.count(2), int(possiblePrime ** 0.5 - 1)):
        if possiblePrime % number == 0:
            return False

    return True


if __name__ == "__main__":
    primes = getPrimes()
    circularPrimes = []
    for prime in primes:
        primeRotations = rotations(prime)
        for rotation in primeRotations:
            if not isPrime(rotation):
                break
        else:
            circularPrimes.append(prime)
    answer = len(circularPrimes)
    print(answer)
