#!/usr/bin/python3

'''

The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)

'''


def decimal(binary):
    '''
    Convert a binary string to a decimal number
    '''
    number = 0
    # Reading binary from right to left
    binary = binary[::-1]
    for bitPos in range(len(binary)):
        bit = binary[bitPos]
        if bit == "1":
            value = 2 ** (bitPos)
            number += value
    return number


def binary(number):
    '''
    Convert a number to a decimal string
    '''
    highestBitValue = 1
    while highestBitValue <= number / 2:
        highestBitValue *= 2

    binary = ""
    bitValue = highestBitValue
    while bitValue > 0.5:
        if bitValue <= number:
            number -= bitValue
            binary += "1"
        else:
            binary += "0"
        bitValue /= 2
    return binary


def palindromes(max):
    '''
    Generate palindromes below a certain number
    '''
    # One digit palindromes
    for x in range(min(10, max)):
        yield x

    def palindromesByDigits(digits, max):
        '''
        Generate palindromes by amount of digits
        '''
        if digits % 2 == 0:
            digits /= 2
            partMin = int(10 ** digits / 10)
            partMax = int(10 ** digits)
            for part in range(partMin, partMax):
                part = str(part)
                palindrome = int(part + part[::-1])
                if palindrome >= max:
                    break
                yield palindrome
        else:
            digits -= 1
            digits /= 2
            partMin = int(10 ** digits / 10)
            partMax = int(10 ** digits)
            for part in range(partMin, partMax):
                part = str(part)
                for midDigit in range(0, 10):
                    palindrome = int(part + str(midDigit) + part[::-1])
                    if palindrome >= max:
                        break
                    yield palindrome

    # Palindromes with more as one digit
    for digit in range(2, len(str(max)) + 1):
        for x in palindromesByDigits(digit, max):
            yield x


def binIsPalindrome(binary):
    '''
    Checks whether a binary string is binary or not
    '''
    partLen = len(binary) // 2
    if len(binary) % 2 == 0:
        return binary[:partLen] == binary[partLen:][::-1]
    else:
        return binary[:partLen] == binary[partLen + 1:][::-1]


def main():
    total = 0
    for palindrome in palindromes(1000000):
        palindromeBinary = binary(palindrome)
        if binIsPalindrome(palindromeBinary):
            total += palindrome
    return(total)


if __name__ == "__main__":
    print(main())
