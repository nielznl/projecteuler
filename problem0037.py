#!/usr/bin/python3

'''

The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right,
and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

'''

primes = [2, 3]


def getPrimes(n):
    # Return all numbers below n, including n that are prime
    global primes
    n += 1
    for x in range(primes[-1] + 2, n, 2):
        for prime in primes[:int(len(primes) ** 0.5) + 1]:
            if x % prime == 0:
                break
        else:
            primes.append(x)


def isPrime(number):
    global primes
    number = int(number)
    return number in primes


def isTruncatable(prime):
    prime = str(prime)
    for digit in range(1, len(prime)):
        if not isPrime(prime[- digit:]) or not isPrime(prime[:digit]):
            break
    else:
        return True
    return False


if __name__ == "__main__":
    getPrimes(1000000)

    truncatablePrimes = []

    for x in primes[4:]:
        if isTruncatable(x):
            truncatablePrimes.append(x)
            print("> " + str(x))
            if len(truncatablePrimes) == 11:
                print(sum(truncatablePrimes))
                break
