#!/usr/bin/python3

import itertools

'''

Take the number 192 and multiply it by each of 1, 2, and 3:

    192 × 1 = 192
    192 × 2 = 384
    192 × 3 = 576

By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?

'''


def is9DigitPandigital(number):
    number = str(number)
    if len(number) != 9:
        return False
    for digit in range(1, 10):
        if str(digit) not in number:
            return False
    return True


def main():
    lastPandigital = ""
    for i in itertools.count(1):
        for numbers in tuple(tuple(range(i, (x + 1) * i, i)) for x in range(1, 10)):
            possiblePandigital = "".join(map(str, numbers))
            if len(numbers) == 2 and len(possiblePandigital) > 9:
                break
            if is9DigitPandigital(possiblePandigital):
                lastPandigital = possiblePandigital
        else:
            continue
        return lastPandigital


if __name__ == "__main__":
    print(main())
