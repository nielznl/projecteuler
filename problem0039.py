#!/usr/bin/python3

'''

If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?

'''


def triangleSolutions(perimeter):
    solutions = 0
    for x in range(1, perimeter):
        for y in range(x, perimeter - x):
            z = (perimeter - x) - y
            if (x**2 + y**2)**0.5 == z:
                solutions += 1
    return solutions


def maximisedSolutionsPermiter(below):
    maximisedSolutions = 0
    maximisedPerimter = 0
    for perimeter in range(1, below):
        solutions = triangleSolutions(perimeter)
        if solutions > maximisedSolutions:
            maximisedSolutions = solutions
            maximisedPerimter = perimeter
    return maximisedPerimter


def main():
    return maximisedSolutionsPermiter(1000)


if __name__ == "__main__":
    print(main())
