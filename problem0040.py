#!/usr/bin/python3

'''

An irrational decimal fraction is created by concatenating the positive integers:

0.123456789101112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

'''


def main():
    fraction = "".join(map(str, tuple(range(1, 1000000))))
    product = 1
    for n in (fraction[10**i - 1] for i in range(0, 7)):
        product *= int(n)
    return product


if __name__ == "__main__":
    print(main())
