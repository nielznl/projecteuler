#!/usr/bin/python3

import itertools

'''

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?

'''


def isPandigital(number):
    number = str(number)
    digits = len(number)
    if digits > 9:
        return False
    for digit in range(1, digits + 1):
        if str(digit) not in number:
            return False
    return True


def isPrime(possiblePrime):
    if possiblePrime < 2:
        return False

    for number in itertools.islice(itertools.count(2), int(possiblePrime ** 0.5 - 1)):
        if possiblePrime % number == 0:
            return False

    return True


def positionIsPrime(limit):
    A = [True for x in range(0, limit)]
    for i in range(2, int(limit ** 0.5)):
        if A[i] is True:
            for j in itertools.count(0):
                j = i * 2 + j * i
                if j < limit:
                    A[j] = False
                else:
                    break
    return A


def getPerms(digits):
    digits.sort()
    for digitPos in range(len(digits)):
        digit = digits[digitPos]
        remainingDigits = digits[:digitPos] + digits[digitPos + 1:]
        if len(digits) > 1:
            remainingDigitCombinations = getPerms(remainingDigits)
        else:
            remainingDigitCombinations = [remainingDigits]
        for remainingDigitCombination in remainingDigitCombinations:
            remainingDigitCombination.insert(0, digit)
            yield remainingDigitCombination


def main():
    largestPandigital = 0
    for n in range(2, 9):
        for perm in getPerms(list(range(1, n))):
            perm = int("".join(map(str, perm)))
            if isPrime(perm):
                largestPandigital = perm
    return largestPandigital


if __name__ == "__main__":
    print(main())
