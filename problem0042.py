#!/usr/bin/python3

import json
import string
import itertools

'''

The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value.
For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using resources/resource0042.txt, a 16K text file containing nearly two-thousand common English words, how many are triangle words?

'''


def importWords():
    words = open("resources/resource0042.txt")
    words = "[" + words.read() + "]"
    words = json.loads(words)
    return words


def wordValue(word):
    value = 0
    for letter in word:
        letterValue = string.ascii_uppercase.find(letter) + 1
        value += letterValue
    return value


def getTriangleNumber(n):
    triangleNumber = 0.5 * n * (n + 1)
    return triangleNumber


def isTriangleWord(word):
    value = wordValue(word)
    for i in itertools.count(1):
        triangleNumber = getTriangleNumber(i)
        if triangleNumber == value:
            return True
        elif triangleNumber > value:
            return False


def main():
    words = importWords()
    triangleWords = 0
    for word in words:
        if isTriangleWord(word):
            triangleWords += 1
    return triangleWords


if __name__ == "__main__":
    print(main())
