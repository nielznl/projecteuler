#!/usr/bin/python3

import itertools

'''
The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

    d2d3d4=406 is divisible by 2
    d3d4d5=063 is divisible by 3
    d4d5d6=635 is divisible by 5
    d5d6d7=357 is divisible by 7
    d6d7d8=572 is divisible by 11
    d7d8d9=728 is divisible by 13
    d8d9d10=289 is divisible by 17

Find the sum of all 0 to 9 pandigital numbers with this property.
'''


def get_primes(limit):                                     
    A = [True for x in range(0, limit)]              
    for i in range(2, int(limit ** 0.5) + 1):
        if A[i] is True:                  
            for j in itertools.count(0):                                          
                j = i * 2 + j * i             
                if j < limit:
                    A[j] = False
                else:
                    break

    primes = []                                                            
    for i in range(2, len(A)):
        if A[i] is True:                                                          
            primes.append(i)                

    return primes


def get_multiples(n):
    l = []
    total = 999 // n
    for i in range(1, total+1):
        x = i*n
        v = str(x).zfill(3)
        l.append(v)

    return l


def main():
    primes = get_primes(18)[::-1]
    numbers = get_multiples(primes[0])
    for prime in primes[1:]:
        new = []
        multiples = get_multiples(prime)
        for multiple in multiples:
            for number in numbers:
                if multiple[1:3] == number[0:2]:
                    x = multiple[0] + number
                    new.append(x)

        numbers = new

    digits = [str(i) for i in range(10)]
    new = []
    for number in numbers:
        if len(set(number)) == 9:
            for digit in digits:
                if digit not in number:
                    new.append(int(digit + number))

    return sum(new)


if __name__ == "__main__":
    print(main())
