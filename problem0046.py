#!/usr/bin/python3

import itertools

from problem0043 import get_primes

'''
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×12
15 = 7 + 2×22
21 = 3 + 2×32
25 = 7 + 2×32
27 = 19 + 2×22
33 = 31 + 2×12


9 7
15 7
21 3
25 7
27 19
33 25
35 3


It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
'''


def get_odd_composites(limit):                                     
    A = [True for x in range(0, limit)]              
    for i in range(2, int(limit ** 0.5)):
        if A[i] is True:                  
            for j in itertools.count(0):                                          
                j = i * 2 + j * i             
                if j < limit:
                    A[j] = False
                else:
                    break

    for i in range(2, len(A)):
        if i % 2 == 1 and A[i] is not True:                                                          
            yield i


def main():
    odd_composites = get_odd_composites(10000)
    for i in range(10000):
        composite = next(odd_composites)
        for prime in get_primes(composite):
            remainder = composite - prime
            half = remainder / 2
            if half ** 0.5 % 1 == 0:
                break
        else:
            return composite
            break

    return


if __name__ == "__main__":
    print(main())
