#!/usr/bin/python3

from itertools import count, islice

from problem0043 import get_primes

'''
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?
'''


def is_prime(n):
    if n < 2:
        return False

    for number in islice(count(2), int(n ** 0.5 - 1)):
        if n % number == 0:
            return False

    return True


def factors(n):
    if is_prime(n):
        return [n]
    
    for i in get_primes(int(n ** 0.5) + 1):
        if n % i == 0:
            return [i] + factors(n // i)


def main():
    n = 4
    streak = 0
    for i in count(2):
        if len(set(factors(i))) == n:
            streak += 1
            if streak == n:
                return i - n + 1
        else:
            streak = 0
        


if __name__ == "__main__":
    print(main())
