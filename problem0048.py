#!/usr/bin/python3

'''
The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.
'''


def main():
    total = 0
    for i in range(1, 1001):
        digits = i ** i % 10 ** 10
        total += digits

    return total % 10 ** 10


if __name__ == "__main__":
    print(main())
