#!/usr/bin/python3

from itertools import permutations

from problem0043 import get_primes

'''
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
'''

primes = [prime for prime in get_primes(10000) if prime > 999]
skip = []


def main():
    for prime in primes:
        perms = [int(''.join(x)) for x in set(permutations(str(prime)))]
        perms = [x for x in perms if x in primes]
        perms.sort()
        if perms[0] in skip:
            continue
        else:
            skip.append(perms[0])

        '''
        for i in range(len(perms) - 2):
            for j in range(i + 1, len(perms) - 1):
                for k in range(j + 1, len(perms)):
                    if perms[j] - perms[i] == perms[k] - perms[j]:

                        print(perms[i], perms[j], perms[k])
        '''
        for i in range(len(perms) - 2):
            p = perms[i]
            a = p + 3330
            b = a + 3330
            if a in perms and b in perms and p != 1487:
                s = str(p) + str(a) + str(b)
                return s


if __name__ == "__main__":
    print(main())
