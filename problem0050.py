#!/usr/bin/python3

from itertools import count

from problem0043 import get_primes
from problem0047 import is_prime

'''
The prime 41, can be written as the sum of six consecutive primes:
41 = 2 + 3 + 5 + 7 + 11 + 13

This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
'''


def gen_primes():
    for i in count(2):
        if is_prime(i):
            yield i


def highest_sum(below):
    prime_gen = gen_primes()
    total = 0
    for i in count(0):
        n = next(prime_gen)
        if total + n > below:
            return total
            break

        total += n


def main():
    h = highest_sum(1000000)
    prime_gen = gen_primes()
    for i in count(0):
        if is_prime(h):
            return h

        h -= next(prime_gen)


if __name__ == "__main__":
    print(main())
